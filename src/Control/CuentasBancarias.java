/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Modelo.Cuenta;
import Util.LeerMatriz_Excel;

/**
 *
 * @author Gederson
 */
public class CuentasBancarias {
    
    private Cuenta myCuenta[];
    
    /**
     * Constructor vacio clase CuentasBancarias
     */
    public CuentasBancarias(){
    }
    
    /**
     * Inicializa el vector thisCuenta recibiendo otro vector tipo Cuenta
     * @param myCuenta Un vector de tipo Cuenta
     */
    public CuentasBancarias(Cuenta[] myCuenta) {
        this.myCuenta = myCuenta;
    }
    
    /**
     * Inicializa el vector thisCuenta recibiendo un archivo excel con las cuentas
     * @param rutaArchivo Ruta del archivo excel
     * @throws Exception Cuando el archivo tiene un error
     */
    public CuentasBancarias(String rutaArchivo) throws Exception{
        LeerMatriz_Excel myExcel = new LeerMatriz_Excel(rutaArchivo,0);
        String datos[][] = myExcel.getMatriz();
        
        this.myCuenta = new Cuenta[datos.length-1];
        crearCuentas(datos);
    }
    
    /**
     * Crea las cuentas y las añade al vector this.myCuenta
     * @param datos Matriz binaria que contiene las cuentas
     * @throws Exception Cuando un dato no es positivo, no es de tipo numérico o no ingresó ningun dato
     */
    private void crearCuentas(String [][]datos) throws Exception{
        for(int fila = 1 ; fila < datos.length ; ++fila){
            Cuenta cuenta = new Cuenta();
            for(int columna = 0 ; columna < datos[fila].length ; ++columna){
                if ("".equals(datos[fila][columna]))
                    throw new Exception("No ingresó ningun dato en la celda " + ++fila + "," + ++columna);
                try{
                    int dato = Integer.parseInt(datos[fila][columna]);
                    if(dato < 0)
                        throw new Exception("El dato " + dato + " no es positivo.");
                    if(columna == 0)
                        cuenta.setId(dato);
                    else
                        cuenta.setCapital(dato);
                }
                catch(java.lang.NumberFormatException ex){
                    throw new Exception("Ingrese solo valores numéricos enteros : " + ex.getMessage());
                }
            }
            this.myCuenta[fila-1] = cuenta;
        }
    }
    
    /**
     * Consigna "x" dinero a una cuenta
     * Ejemplo : cuenta: 1 , dinero: 100 ----> La cuenta numero 1 ahora tiene 100 más en capital
     * @param idCuenta El número de cuenta a consignar
     * @param dinero El dinero que se va a consignar
     */
    public void conisgnar(int idCuenta, int dinero){
        this.myCuenta[idCuenta-1].consignar(dinero);
    }
    
    /**
     * La cuenta "idCuenta" retira el monto "dinero"
     * Ejemplo : {cuenta : 1 , getCapital : 100} , dinero : 50 ---> La cuenta numero 1 ahora tiene 50 en su capital.
     * @param idCuenta El número de la cuenta que va a retirar dinero
     * @param dinero La cantidad de dinero a retirar
     * @throws Exception Una excepcion cuando la cuenta tenga menos monto que la cantidad de dinero
     */
    public void retirar(int idCuenta, int dinero)throws Exception{
        if(this.myCuenta[idCuenta-1].getCapital() < dinero)
            throw new Exception("La cuenta No " + idCuenta + " tiene menor monto disponible : " + this.myCuenta[idCuenta-1].getCapital() + " < " + dinero);
        this.myCuenta[idCuenta-1].retirar(dinero);
    }
    
    /**
     * La cuenta número "idCuenta1" transfiere el dinero "dinero"  a la cuenta numero "idCuenta2"
     * Ejemplo : cuenta : 1 , getCapital : 100
     *           cuenta : 2 , getCapital : 50
     *           dinero : 50 ----> La cuenta numero 1 ahora tiene 50 en su capital y la cuenta numero 2 tiene 100 en su capital
     * @param idCuenta1 El número de la cuenta que va a transferir el dinero
     * @param idCuenta2 La cuenta que recibirá el dinero
     * @param dinero El dinero que se va a tranferir
     * @throws Exception Una excepcion cuando la cuenta1 tenga menos monto que la cantidad de dinero
     */
    public void transferir(int idCuenta1, int idCuenta2, int dinero)throws Exception{
        retirar(idCuenta1, dinero);
        this.myCuenta[idCuenta2-1].consignar(dinero);
    }
    
     /**
     * Comprueba si una cuenta existe en el vector de Cuentas
     * @param cuenta El número de la cuenta
     * @throws Exception Genera Excepcion cuando la cuenta no existe
     */
    public void cuentaExiste(int cuenta)throws Exception{
        if(cuenta < 0 || cuenta > this.myCuenta.length-1)
            throw new Exception("La cuenta ingresada no existe.");
    }
    
    /**
     * Comprueba si el dinero ingresado es válido (No es lógico que un cajero opere con dinero nulo (0) o negativo).
     * @param dinero Cantidad de dinero
     * @throws Exception Genera excepcion cuando el dinero es nulo (0) o Negativo.
     */
    public void dineroValido(int dinero)throws Exception{
        if(dinero <= 0)
            throw new Exception("El dinero ingresado debe ser positivo.");
    }
    
    @Override
    public String toString(){
        String msg = "";
        
        for(Cuenta dato:this.myCuenta)
            msg += dato.toString() + "\n";
        
        return msg;
    }
    
    public Cuenta[] getMyCuenta() {
        return myCuenta;
    }

    public void setMyCuenta(Cuenta[] myCuenta) {
        this.myCuenta = myCuenta;
    }
}