/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Gederson
 */
public class Cuenta {
    
    private int id;
    private int capital;
    
    /**
     * Constructor vacio tipo Cuenta
     */
    public Cuenta(){
    }
    
    /**
     * Inicializar la Cuenta con valores especificos
     * @param id Numero de la cuenta
     * @param capital Dinero de la cuenta
     */
    public Cuenta(int id, int capital) {
        this.id = id;
        this.capital = capital;
    }
    
    /**
     * Agrega dinero a la cuenta
     * @param dinero Cantidad de dinero
     */
    public void consignar(int dinero){
        this.capital += dinero;
    }
    
    /**
     * Resta dinero a la cuenta
     * @param dinero Cantidad de dinero
     */
    public void retirar(int dinero){
        this.capital -= dinero;
    }
    
    @Override
    public String toString() {
        return " La Cuenta No" + id + " tiene el capital : " + capital;
    }

 
    public int getId() {
        return this.id;
    }

    public int getCapital() {
        return this.capital;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCapital(int capital) {
        this.capital = capital;
    }

}
