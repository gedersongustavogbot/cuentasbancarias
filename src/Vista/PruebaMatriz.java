/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import Control.CuentasBancarias;
import java.util.Scanner;
/**
 *
 * @author Gederson
 */
public class PruebaMatriz {
    
    public static void main(String[] args)throws Exception{
        try{
        //Crear cuentas
        CuentasBancarias myBanco = new CuentasBancarias("src/Datos/Cuentas.xls");
        System.out.println("Las cuentas bancarias son :\n" + myBanco.toString());
        
        Scanner lector = new Scanner(System.in);
        //Accion a ejecutar en el cajero, por defecto 0
        int accion = 0;
        //Ciclo se ejecuta mientras el usuario no desee finalizar sus operaciones.
        while(accion != 4){
            System.out.println("¿Qué desea hacer?\n" + "Ingrese uno de los siguientes números (1,2,3 o 4): \n" 
                            + "1 - Consignar\n" + "2 - Retirar\n" + "3 - Transferir\n" + "4 - Salir\n");
            accion = lector.nextInt();
            //Verificar si la acción es válida
            if(accion < 1 || accion > 3)
                throw new Exception("Debe digitar un número entre 1 y 4.");
            
            //Si desea consignar o retirar deberá ingresar una cuenta y un dinero.
            //En cada operacion se verificará si la cuenta ingresada existe y el dinero ingresado es válido.
            if(accion == 1 || accion == 2){
            System.out.println("Por favor ingrese el número de cuenta :");
            int cuenta = lector.nextInt();
            myBanco.cuentaExiste(cuenta);
            System.out.println("Por favor ingrese la cantidad de dinero :");
            int dinero = lector.nextInt();
            myBanco.dineroValido(dinero);
            //Si desea consignar entra aquí
            if(accion == 1){
                myBanco.conisgnar(cuenta, dinero);
                System.out.println("¡Consignacion completa!\n" + myBanco.toString());  //:)
            }
            //Si desea retirar entra aquí
            else{
                myBanco.retirar(cuenta, dinero);
                System.out.println("¡Retiro completo!\n" + myBanco.toString());  //:)
            }
            }
            //Si desea transferir entra aquí
            if(accion == 3){
                    System.out.println("Por favor ingrese el numero de cuenta desde la cual va a transferir :");
                    int cuenta = lector.nextInt();
                    myBanco.cuentaExiste(cuenta);
                    System.out.println("Por favor ingrese el numero de cuenta a la que se va a transferir :");
                    int cuenta2 = lector.nextInt();
                    myBanco.cuentaExiste(cuenta2);
                    System.out.println("Por favor ingrese el dinero que se va a transferir :");
                    int dinero = lector.nextInt();
                    myBanco.dineroValido(dinero);
                    myBanco.transferir(cuenta, cuenta2, dinero);
                    System.out.println("¡Transferencia exitosa!\n" + myBanco.toString());
            }
        }
        //Si desea salir entra aquí
        System.out.println("Operaciones terminadas."); //:)
        }
        //Multicatch para excepciones de formatos
        catch(java.lang.NumberFormatException | java.util.InputMismatchException ex){
            System.err.println("Debe digitar solo valores de tipo numérico entero: " + ex.getMessage());
        }
        catch(Exception ex){
            System.err.println("Ha ocurrido un error : " + ex.getMessage());
        }
    }
}
